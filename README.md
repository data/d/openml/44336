# OpenML dataset: Meta_Album_PLT_DOC_Extended

https://www.openml.org/d/44336

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Plant Doc Dataset (Extended)**
***
The PlantDoc dataset(https://github.com/pratikkayal/PlantDoc-Dataset) is made up of images of leaves of healthy and unhealthy plants. The images were downloaded from Google Images and Ecosia, and later cropped by the authors, so generally, one complete leaf fits in one image. The original, uncropped images are generally different in scale, light conditions, and pose. However, within one category, images of leaves that came from the same original image can be found. The images correspond to 27 classes, including plant disease names and plant species names, e.g.: Corn Leaf Blight and Cherry Leaf respectively. The dataset was created for a benchmarking classification model work, published in 2020 by Singh et al. The PlantDoc dataset in the Meta-Album benchmark is extracted from a preprocessed version of the original PlantDoc dataset. First, to get i.i.d. samples, only one leaf image per each original image is randomly picked. Then, leaves images are cropped and made into squared images which are then resized into 128x128 with anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/PLT_DOC.png)

**Meta Album ID**: PLT_DIS.PLT_DOC  
**Meta Album URL**: [https://meta-album.github.io/datasets/PLT_DOC.html](https://meta-album.github.io/datasets/PLT_DOC.html)  
**Domain ID**: PLT_DIS  
**Domain Name**: Plant Diseases  
**Dataset ID**: PLT_DOC  
**Dataset Name**: Plant Doc  
**Short Description**: Plant disease dataset  
**\# Classes**: 27  
**\# Images**: 2549  
**Keywords**: plants, plant diseases,  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Creative Commons Attribution 4.0 International  
**License URL(original data release)**: https://github.com/pratikkayal/PlantDoc-Object-Detection-Dataset/blob/master/LICENSE.txt
 
**License (Meta-Album data release)**: Creative Commons Attribution 4.0 International  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)  

**Source**: PlantDoc: A Dataset for Visual Plant Disease Detection  
**Source URL**: https://github.com/pratikkayal/PlantDoc-Object-Detection-Dataset  
  
**Original Author**: Sharada Mohanty, David Hughes, and Marcel Salathe  
**Original contact**:   

**Meta Album author**: Maria Belen Guaranda Cabezas  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@inproceedings{10.1145/3371158.3371196,
    author = {Singh, Davinder and Jain, Naman and Jain, Pranjali and Kayal, Pratik and Kumawat, Sudhakar and Batra, Nipun},
    title = {PlantDoc: A Dataset for Visual Plant Disease Detection},
    year = {2020},
    isbn = {9781450377386},
    publisher = {Association for Computing Machinery},
    address = {New York, NY, USA},
    url = {https://doi.org/10.1145/3371158.3371196},
    doi = {10.1145/3371158.3371196},
    booktitle = {Proceedings of the 7th ACM IKDD CoDS and 25th COMAD},
    pages = {249-253},
    numpages = {5},
    keywords = {Object Detection, Image Classification, Deep Learning},
    location = {Hyderabad, India},
    series = {CoDS COMAD 2020}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44273)  [[Mini]](https://www.openml.org/d/44303)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44336) of an [OpenML dataset](https://www.openml.org/d/44336). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44336/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44336/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44336/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

